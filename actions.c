#include <string.h>
#include "mytypes.h"

void A_L(const struct case_string *from_case, struct case_string *to_case)
{
  *to_case = *from_case;

  to_case->corner.tl = (from_case->corner.tl + 1) % 3;
  to_case->corner.br = (from_case->corner.br + 1) % 3;
  to_case->corner.tr = (from_case->corner.tr + 1) % 3;

  to_case->edge.t = (from_case->edge.r + 3) % 4;
  to_case->edge.l = (from_case->edge.t + 3) % 4;
  to_case->edge.r = (from_case->edge.l + 2) % 4;

  to_case->yellow.t = from_case->yellow.r;
  to_case->yellow.l = from_case->yellow.t;
  to_case->yellow.r = from_case->yellow.l;
}

void A_L_prime(const struct case_string *from_case, struct case_string *to_case)
{
  *to_case = *from_case;

  to_case->corner.tl = (from_case->corner.tl + 2) % 3;
  to_case->corner.br = (from_case->corner.br + 2) % 3;
  to_case->corner.tr = (from_case->corner.tr + 2) % 3;

  to_case->edge.t = (from_case->edge.l + 1) % 4;
  to_case->edge.l = (from_case->edge.r + 2) % 4;
  to_case->edge.r = (from_case->edge.t + 1) % 4;

  to_case->yellow.t = from_case->yellow.l;
  to_case->yellow.l = from_case->yellow.r;
  to_case->yellow.r = from_case->yellow.t;
}

void B_L(const struct case_string *from_case, struct case_string *to_case)
{
  *to_case = *from_case;

  to_case->corner.tl = (from_case->corner.tl + 1) % 3;
  to_case->corner.bl = (from_case->corner.bl + 1) % 3;
  to_case->corner.br = (from_case->corner.br + 1) % 3;

  to_case->edge.t = (from_case->edge.b + 2) % 4;
  to_case->edge.l = (from_case->edge.t + 3) % 4;
  to_case->edge.b = (from_case->edge.l + 3) % 4;

  to_case->yellow.t = (from_case->yellow.b + 1) % 2;
  to_case->yellow.l = from_case->yellow.t;
  to_case->yellow.b = (from_case->yellow.l + 1) % 2;
}

void B_L_prime(const struct case_string *from_case, struct case_string *to_case)
{
  *to_case = *from_case;

  to_case->corner.tl = (from_case->corner.tl + 2) % 3;
  to_case->corner.bl = (from_case->corner.bl + 2) % 3;
  to_case->corner.br = (from_case->corner.br + 2) % 3;

  to_case->edge.t = (from_case->edge.l + 1) % 4;
  to_case->edge.l = (from_case->edge.b + 1) % 4;
  to_case->edge.b = (from_case->edge.t + 2) % 4;

  to_case->yellow.t = from_case->yellow.l;
  to_case->yellow.l = (from_case->yellow.b + 1) % 2;
  to_case->yellow.b = (from_case->yellow.t + 1) % 2;
}

void B2_L(const struct case_string *from_case, struct case_string *to_case)
{
  *to_case = *from_case;

  to_case->corner.tl = (from_case->corner.tl + 1) % 3;
  to_case->corner.bl = (from_case->corner.bl + 1) % 3;
  to_case->corner.tr = (from_case->corner.tr + 1) % 3;

  to_case->edge.t = (from_case->edge.b + 2) % 4;
  to_case->edge.l = (from_case->edge.t + 3) % 4;
  to_case->edge.b = (from_case->edge.l + 3) % 4;

  to_case->yellow.t = (from_case->yellow.b + 1) % 2;
  to_case->yellow.l = from_case->yellow.t;
  to_case->yellow.b = (from_case->yellow.l + 1) % 2;
}

void B2_L_prime(const struct case_string *from_case, struct case_string *to_case)
{
  *to_case = *from_case;

  to_case->corner.tl = (from_case->corner.tl + 2) % 3;
  to_case->corner.bl = (from_case->corner.bl + 2) % 3;
  to_case->corner.tr = (from_case->corner.tr + 2) % 3;

  to_case->edge.t = (from_case->edge.l + 1) % 4;
  to_case->edge.l = (from_case->edge.b + 1) % 4;
  to_case->edge.b = (from_case->edge.t + 2) % 4;

  to_case->yellow.t = from_case->yellow.l;
  to_case->yellow.l = (from_case->yellow.b + 1) % 2;
  to_case->yellow.b = (from_case->yellow.t + 1) % 2;
}

void C_L(const struct case_string *from_case, struct case_string *to_case)
{
  *to_case = *from_case;

  to_case->corner.tl = (from_case->corner.tl + 1) % 3;
  to_case->corner.br = (from_case->corner.br + 1) % 3;
  to_case->corner.tr = (from_case->corner.tr + 1) % 3;

  to_case->edge.l = (from_case->edge.b + 1) % 4;
  to_case->edge.b = (from_case->edge.r + 1) % 4;
  to_case->edge.r = (from_case->edge.l + 2) % 4;

  to_case->yellow.l = (from_case->yellow.b + 1) % 2;
  to_case->yellow.b = (from_case->yellow.r + 1) % 2;
  to_case->yellow.r = from_case->yellow.l;
}

void C_L_prime(const struct case_string *from_case, struct case_string *to_case)
{
  *to_case = *from_case;

  to_case->corner.tl = (from_case->corner.tl + 2) % 3;
  to_case->corner.br = (from_case->corner.br + 2) % 3;
  to_case->corner.tr = (from_case->corner.tr + 2) % 3;

  to_case->edge.l = (from_case->edge.r + 2) % 4;
  to_case->edge.b = (from_case->edge.l + 3) % 4;
  to_case->edge.r = (from_case->edge.b + 3) % 4;

  to_case->yellow.l = from_case->yellow.r;
  to_case->yellow.b = (from_case->yellow.l + 1) % 2;
  to_case->yellow.r = (from_case->yellow.b + 1) % 2;
}

void D_double(const struct case_string *from_case, struct case_string *to_case)
{
  *to_case = *from_case;

  to_case->corner.tl = (from_case->corner.tl + 2) % 3;
  to_case->corner.br = (from_case->corner.br + 1) % 3;
}

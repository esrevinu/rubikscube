#ifndef __MY_TYPES__
#define __MY_TYPES__

struct corner_string {
  unsigned char tl : 2;
  unsigned char bl : 2;
  unsigned char br : 2;
  unsigned char tr : 2;
};

struct edge_string {
  unsigned char t : 2;
  unsigned char l : 2;
  unsigned char b : 2;
  unsigned char r : 2;
};

struct yellow_string {
  unsigned char t : 1;
  unsigned char l : 1;
  unsigned char b : 1;
  unsigned char r : 1;
};

struct case_string {
  struct corner_string corner;
  struct edge_string edge;
  struct yellow_string yellow;
};

struct trans_info {
  const char *action;
  int rot; /* rotation for action */
  struct case_info *to;
  int rot2; /* rotation for case matching */
};

struct case_info {
  struct case_string case_string;
  struct case_info *parity;
  int parity_rot;
  int solve_number;
  int trans_number;
  struct trans_info *trans;
};

#endif /* __MY_TYPES__ */

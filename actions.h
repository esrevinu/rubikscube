#ifndef __ACTIONS__
#define __ACTIONS__

#include "mytypes.h"

typedef void (*action_func)(const struct case_string *from_case,
                            struct case_string *to_case);

void A_L(const struct case_string *from_case,
         struct case_string *to_case);
void A_L_prime(const struct case_string *from_case,
               struct case_string *to_case);
void B_L(const struct case_string *from_case,
         struct case_string *to_case);
void B_L_prime(const struct case_string *from_case,
               struct case_string *to_case);
void B2_L(const struct case_string *from_case,
          struct case_string *to_case);
void B2_L_prime(const struct case_string *from_case,
                struct case_string *to_case);
void C_L(const struct case_string *from_case,
         struct case_string *to_case);
void C_L_prime(const struct case_string *from_case,
               struct case_string *to_case);
void D_double(const struct case_string *from_case,
              struct case_string *to_case);

#endif /* __ACTIONS__ */

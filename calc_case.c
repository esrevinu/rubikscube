#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mytypes.h"
#include "actions.h"

void print_case(struct case_string cs);
int check_uniq(struct case_info **uniq_cases, int *parity_rot,
               struct corner_string c, struct edge_string e,
               struct yellow_string y);
void parity_transform(struct case_string *cs);
void find_solvable(struct case_info **uniq_cases, action_func action,
                   int solve_number, int add, const char *action_name,
                   const char *action_name2);

int main() {
  struct corner_string case_corner[8] = {
      {0, 0, 0, 0}, {0, 1, 2, 0}, {0, 2, 1, 0}, {0, 1, 0, 2},
      {1, 0, 1, 1}, {2, 2, 0, 2}, {1, 1, 2, 2}, {1, 2, 1, 2}};
  struct edge_string case_edge[12] = {
      {0, 0, 0, 0}, {0, 1, 1, 2}, {1, 1, 2, 0}, {1, 2, 0, 1},
      {2, 0, 1, 1}, {0, 2, 3, 3}, {2, 3, 3, 0}, {3, 3, 0, 2},
      {3, 0, 2, 3}, {1, 3, 1, 3}, {3, 1, 3, 1}, {2, 2, 2, 2}};
  struct yellow_string case_yellow[8] = {
      {0, 0, 0, 0}, {0, 0, 1, 1}, {0, 1, 1, 0}, {1, 1, 0, 0},
      {1, 0, 0, 1}, {0, 1, 0, 1}, {1, 0, 1, 0}, {1, 1, 1, 1}};
  struct case_info *uniq_cases[768] = {NULL};
  int count = 0;

  for (int i = 0; i < 8; i++) {
    for (int j = 0; j < 12; j++) {
      for (int k = 0; k < 8; k++) {
        int parity_rot = -1;
        int parity = check_uniq(uniq_cases, &parity_rot,
                                case_corner[i], case_edge[j], case_yellow[k]);
        if (parity) {
          struct case_info *another_case = malloc(sizeof(struct case_info));
          memset(another_case, 0, sizeof(struct case_info));
          another_case->case_string =
            (struct case_string) {case_corner[i], case_edge[j], case_yellow[k]};
          another_case->solve_number = -1;
          another_case->parity_rot = parity_rot;
          uniq_cases[count++] = another_case;
          if (parity > 0) {
            another_case->parity = uniq_cases[parity - 1];
            another_case->parity->parity_rot = parity_rot;
            another_case->parity->parity = another_case;
          }
        }
      }
    }
  }

  /* 0000 0000 0000 */
  uniq_cases[0]->solve_number = 0;

  /* 1-action solvable */
  find_solvable(uniq_cases, A_L_prime, 1, 1, "A_L", "A_R");
  find_solvable(uniq_cases, A_L, 1, 1, "A_L_prime", "A_R_prime");
  find_solvable(uniq_cases, B_L_prime, 1, 1, "B_L", "B_R");
  find_solvable(uniq_cases, B_L, 1, 1, "B_L_prime", "B_R_prime");
  find_solvable(uniq_cases, B2_L_prime, 1, 1, "B2_L", "B2_R");
  find_solvable(uniq_cases, B2_L, 1, 1, "B2_L_prime", "B2_R_prime");
  find_solvable(uniq_cases, C_L_prime, 1, 1, "C_L", "C_R");
  find_solvable(uniq_cases, C_L, 1, 1, "C_L_prime", "C_R_prime");

  /* 2-action solvable */
  find_solvable(uniq_cases, A_L_prime, 2, 1, "A_L", "A_R");
  find_solvable(uniq_cases, A_L, 2, 1, "A_L_prime", "A_R_prime");
  find_solvable(uniq_cases, B_L_prime, 2, 1, "B_L", "B_R");
  find_solvable(uniq_cases, B_L, 2, 1, "B_L_prime", "B_R_prime");
  find_solvable(uniq_cases, B2_L_prime, 2, 1, "B2_L", "B2_R");
  find_solvable(uniq_cases, B2_L, 2, 1, "B2_L_prime", "B2_R_prime");
  find_solvable(uniq_cases, C_L_prime, 2, 1, "C_L", "C_R");
  find_solvable(uniq_cases, C_L, 2, 1, "C_L_prime", "C_R_prime");
  find_solvable(uniq_cases, D_double, 2, 2, "D_double", NULL);

  /* 3-action solvable */
  find_solvable(uniq_cases, A_L_prime, 3, 1, "A_L", "A_R");
  find_solvable(uniq_cases, A_L, 3, 1, "A_L_prime", "A_R_prime");
  find_solvable(uniq_cases, B_L_prime, 3, 1, "B_L", "B_R");
  find_solvable(uniq_cases, B_L, 3, 1, "B_L_prime", "B_R_prime");
  find_solvable(uniq_cases, B2_L_prime, 3, 1, "B2_L", "B2_R");
  find_solvable(uniq_cases, B2_L, 3, 1, "B2_L_prime", "B2_R_prime");
  find_solvable(uniq_cases, C_L_prime, 3, 1, "C_L", "C_R");
  find_solvable(uniq_cases, C_L, 3, 1, "C_L_prime", "C_R_prime");
  find_solvable(uniq_cases, D_double, 3, 2, "D_double", NULL);

  /* 4-action solvable */
  find_solvable(uniq_cases, A_L_prime, 4, 1, "A_L", "A_R");
  find_solvable(uniq_cases, A_L, 4, 1, "A_L_prime", "A_R_prime");
  find_solvable(uniq_cases, B_L_prime, 4, 1, "B_L", "B_R");
  find_solvable(uniq_cases, B_L, 4, 1, "B_L_prime", "B_R_prime");
  find_solvable(uniq_cases, B2_L_prime, 4, 1, "B2_L", "B2_R");
  find_solvable(uniq_cases, B2_L, 4, 1, "B2_L_prime", "B2_R_prime");
  find_solvable(uniq_cases, C_L_prime, 4, 1, "C_L", "C_R");
  find_solvable(uniq_cases, C_L, 4, 1, "C_L_prime", "C_R_prime");
  find_solvable(uniq_cases, D_double, 4, 2, "D_double", NULL);

  for (int i = 0; uniq_cases[i]; i++) {
    printf("%04d: ", i + 1);
    print_case(uniq_cases[i]->case_string);
    if (uniq_cases[i]->parity) {
      printf(" Parity_%d to ", uniq_cases[i]->parity_rot);
      print_case(uniq_cases[i]->parity->case_string);
    } else {
      printf(" Parity_%d to Self", uniq_cases[i]->parity_rot);
    }
    printf("\n   (%d)", uniq_cases[i]->solve_number);
    if (uniq_cases[i]->trans) {
      for (int j = 0; j < uniq_cases[i]->trans_number; j++) {
        printf(" | %s_%d_%d -> ", uniq_cases[i]->trans[j].action,
               uniq_cases[i]->trans[j].rot, uniq_cases[i]->trans[j].rot2);
        print_case(uniq_cases[i]->trans[j].to->case_string);
      }
    }
    printf("\n");
  }

  return 0;
}

void print_case(struct case_string cs) {
  printf("%d%d%d%d %d%d%d%d %d%d%d%d",
         cs.corner.tl, cs.corner.bl, cs.corner.br, cs.corner.tr,
         cs.edge.t, cs.edge.l, cs.edge.b, cs.edge.r,
         cs.yellow.t, cs.yellow.l, cs.yellow.b, cs.yellow.r);
}

void rotate(struct case_string *cs)
{
  struct case_string buf = *cs;

  cs->corner.tl = buf.corner.bl;
  cs->corner.bl = buf.corner.br;
  cs->corner.br = buf.corner.tr;
  cs->corner.tr = buf.corner.tl;

  cs->edge.t = buf.edge.l;
  cs->edge.l = buf.edge.b;
  cs->edge.b = buf.edge.r;
  cs->edge.r = buf.edge.t;

  cs->yellow.t = buf.yellow.l;
  cs->yellow.l = buf.yellow.b;
  cs->yellow.b = buf.yellow.r;
  cs->yellow.r = buf.yellow.t;
}

/**
   action is an inverse action.
   action_name2 is nullable.

   phi_Lf = R_2 A_L^-1 R_1 phi_Lt
   phi_Lt = R_1^-1 A_L R_2^-1 phi_Lf
          = R(a_1) A_L R(a_2) phi_Lf
   phi_R = R_p P phi_L
   phi_L = P R_p^-1 phi_R = P R(a_p) phi_R = R_p P phi_R = R(-a_p) P phi_R
   phi_Rt = R_pt P phi_Lt
          = R_pt P R_1^-1 A_L R_2^-1 phi_Lf
          = R_pt P R_1^-1 A_L R_2^-1 P R_pf^-1 phi_Rf
          = R_pt (P R_1^-1 P) (P A_L P) (P R_2^-1 P) R_pf^-1 phi_Rf
          = R_pt R_1 A_R R_2 R_pf^-1 phi_Rf
          = R(-a_1 - a_pt) A_R R(-a_2 + a_pf) phi_Rf
 */
void find_solvable(struct case_info **uniq_cases, action_func action,
                   int solve_number, int add,
                   const char *action_name, const char *action_name2)
{
  int target_solve_number = solve_number - add;
  struct case_string from_buf, to_buf, buf;

  for (int i = 0; uniq_cases[i] != NULL; i++) {
    struct case_info *target_case = uniq_cases[i];
    if (target_case->solve_number != target_solve_number) continue;

    from_buf = target_case->case_string;
    /* For a single kind of action, we can apply it in 4 directions. */
    for (int rot = 0; rot < 4; rot++) {
      if (rot) {
        rotate(&from_buf);
        /* If the case has a symmetry,
           we don't have to consider every rotation */
        if (memcmp(&from_buf, &target_case->case_string,
                   sizeof(struct case_string)) == 0) break;
      }

      action(&from_buf, &to_buf);

      buf = to_buf;
      for (int rot2 = 0; rot2 < 4; rot2++) {
        int found = 0;
        if (rot2) {
          rotate(&to_buf);
          if (memcmp(&to_buf, &buf, sizeof(struct case_string)) == 0) break;
        }

        for (int j = 0; uniq_cases[j] != NULL; j++) {
          struct case_info *found_case = uniq_cases[j];
          if (found_case->solve_number != -1 &&
              found_case->solve_number != solve_number) continue;
          if (memcmp(&found_case->case_string, &to_buf,
                     sizeof(struct case_string)) == 0) {
            struct trans_info *p;
            found_case->solve_number = solve_number;
            found_case->trans_number++;
            found_case->trans =
              realloc(found_case->trans,
                      sizeof(struct trans_info) * found_case->trans_number);
            p = found_case->trans + found_case->trans_number - 1;
            p->action = action_name;
            p->rot = rot2;
            p->to = target_case;
            p->rot2 = rot;
            if (action_name2) {
              if(found_case->parity) {
                found_case->parity->solve_number = solve_number;
                found_case->parity->trans_number++;
                found_case->parity->trans =
                    realloc(found_case->parity->trans,
                            sizeof(struct trans_info) *
                                found_case->parity->trans_number);
                p = found_case->parity->trans +
                    found_case->parity->trans_number - 1;
                p->action = action_name2;
                p->rot = (4 - rot2 + found_case->parity_rot) % 4;
                p->to = target_case->parity ? target_case->parity : target_case;
                p->rot2 = (8 - rot - target_case->parity_rot) % 4;
              } else {
                found_case->trans_number++;
                found_case->trans =
                    realloc(found_case->trans, sizeof(struct trans_info) *
                                                   found_case->trans_number);
                p = found_case->trans + found_case->trans_number - 1;
                p->action = action_name2;
                p->rot = (4 - rot2 + found_case->parity_rot) % 4;
                p->to = target_case->parity ? target_case->parity : target_case;
                p->rot2 = (8 - rot - target_case->parity_rot) % 4;
              }
            }
            found = 1;
            break;
          }
        }
        if (found) break;
      }
    }
  }
}

int check_uniq(struct case_info **uniq_cases, int *parity_rot,
               struct corner_string c, struct edge_string e,
               struct yellow_string y)
{
  struct case_string buf1, buf2, buf3;

  buf1 = buf2 = (struct case_string) {c, e, y};

  for (int rot = 0; rot < 4; rot++) {
    if (rot) {
      rotate(&buf2);
      if (memcmp(&buf1, &buf2, sizeof(struct case_string)) == 0) break;
    }

    for (int j = 0; uniq_cases[j] != NULL; j++) {
      if (memcmp(&uniq_cases[j]->case_string, &buf2,
                 sizeof(struct case_string)) == 0)
        return 0;
    }
  }

  /* check parity */

  buf2 = buf1;
  parity_transform(&buf2);
  buf3 = buf2;

  for (int rot = 0; rot < 4; rot++) {
    if (rot) {
      rotate(&buf3);
      if (memcmp(&buf2, &buf3, sizeof(struct case_string)) == 0) break;
    }

    if (memcmp(&buf1, &buf3, sizeof(struct case_string)) == 0) {
      *parity_rot = rot;
      return -1;
    }
    for (int j = 0; uniq_cases[j] != NULL; j++) {
      if (uniq_cases[j]->parity_rot < 0 &&
          memcmp(&uniq_cases[j]->case_string,
                 &buf3, sizeof(struct case_string)) == 0) {
        *parity_rot = rot;
        return j + 1;
      }
    }
  }

  return -1;
}

void parity_transform(struct case_string *cs)
{
  struct case_string buf = *cs;

  cs->corner.tl = (3 - buf.corner.tr) % 3;
  cs->corner.bl = (3 - buf.corner.br) % 3;
  cs->corner.br = (3 - buf.corner.bl) % 3;
  cs->corner.tr = (3 - buf.corner.tl) % 3;

  cs->edge.t = (4 - buf.edge.t) % 4;
  cs->edge.l = (4 - buf.edge.r) % 4;
  cs->edge.b = (4 - buf.edge.b) % 4;
  cs->edge.r = (4 - buf.edge.l) % 4;

  cs->yellow.l = buf.yellow.r;
  cs->yellow.r = buf.yellow.l;
}
